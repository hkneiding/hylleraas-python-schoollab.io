{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "focal-exhibit",
   "metadata": {},
   "source": [
    "# Get to know the tmQM Dataset: Statistics & Visualizations\n",
    "\n",
    "In this notebook we will look at basic techniques to analyze and visualize data. \n",
    "\n",
    "We will use the [tmQM dataset](https://pubs.acs.org/doi/10.1021/acs.jcim.0c01041) in this notebook. The tmQM dataset provides the quantum properties of 86k transition metal complexes (TMC) representing the 3d, 4d, and 5d series. Structures were extracted from the [Cambridge Structural Database](https://www.ccdc.cam.ac.uk/solutions/csd-core/components/csd/) (CSD) with a series of filters yielding complexes with a single metal center. tmQM contains organometallic, bioinorganic, and Werner complexes. The dataset is distributed in four different files containing different quantum properties that can be used as either descriptors or targets in machine learning models. These properties were computed for the closed-shell singlet state at two different levels of theory; i.e. GFN2-xTB and TPSSh/def2TZVP.\n",
    "\n",
    "<div>\n",
    "<img src=\"https://user-images.githubusercontent.com/51946437/91875604-fabc5300-ec7b-11ea-9b0d-b6b308dc942b.png\" width=\"800\">\n",
    "</div>\n",
    "\n",
    "In this tutorial, we will use two `.csv` files of tmQM containing different information about the TMCs. For each TMC (identified by the six-character code of the CSD), `tmQM_quantum_properties.csv` provides the following quantum properties: electronic energy, dispersion energy, dipole moment, metal center charge, HOMO energy, LUMO energy, HOMO-LUMO gap, and polarizability. `tmQM_additional_data.csv` contains more general information: the charge, group and period of the metal center,\tcoordination number, and the number of atoms and electrons.\n",
    "\n",
    "Given the large size of the dataset, Python is an excellent tool to explore, analyze and understand the data using statistics and visualization methods.\n",
    "\n",
    "#### Requirements for this notebook:\n",
    " 1. Python3 or higher\n",
    " 2. Libraries: [numpy](https://numpy.org/), [pandas](https://pandas.pydata.org/), [matplotlib](https://matplotlib.org/)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "incorporate-holder",
   "metadata": {},
   "source": [
    "Run this cell to install all the package dependencies."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "upset-cowboy",
   "metadata": {},
   "outputs": [],
   "source": [
    "# install dependencies\n",
    "!pip3 install numpy pandas matplotlib"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b11a2f66",
   "metadata": {},
   "source": [
    "First, we start by importing a couple of libraries commonly used for basic data analysis and visualisation. Furthermore, we configure the matplotlib plotting mode."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b732d6e0",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "# ensures plots are shown within the notebook\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "comparative-hands",
   "metadata": {},
   "source": [
    "With pandas we can easily import CSV formatted files as [pandas.DataFrame](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html) objects, essentially providing a table. The command `.head()` shows the first 5 lines of the table and the appropriate headings."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cd1dfad0",
   "metadata": {},
   "outputs": [],
   "source": [
    "data_y = pd.read_csv('./tmQM_quantum_properties.csv', sep=',')\n",
    "data_y.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "db646ead",
   "metadata": {},
   "outputs": [],
   "source": [
    "# TODO: read the CSV file 'tmQM_additional_data.csv' and visualize the first lines.\n",
    "data_ad = pd.read_csv('./tmQM_additional_data.csv', sep=',')\n",
    "data_ad.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2e154d78",
   "metadata": {},
   "source": [
    "Note that you can also directly import Excel sheets with the [pandas.read_excel](https://pandas.pydata.org/docs/reference/api/pandas.read_excel.html) function."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "79da7d30",
   "metadata": {},
   "source": [
    "If the entries in the two files are linked via common identifiers we can use [pandas.DataFrame.merge](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.merge.html) to easily merge their contents into one DataFrame. In case of tmQM, each entry is uniquely identified by its CSD code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2c5e1f7f",
   "metadata": {},
   "outputs": [],
   "source": [
    "# merge data from both files into one DataFrame\n",
    "data = data_y.merge(data_ad, how='inner', on='CSD_code')\n",
    "data.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "balanced-average",
   "metadata": {},
   "source": [
    "We can select specific entries using pandas query functions. Each selection will return a new dataframe with the appropriate subset. ([Full documentation for sampling dataframes](https://pandas.pydata.org/docs/getting_started/intro_tutorials/03_subset_data.html))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "incorporate-title",
   "metadata": {},
   "outputs": [],
   "source": [
    "# get a column by name\n",
    "print(data['CSD_code'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "northern-olympus",
   "metadata": {},
   "outputs": [],
   "source": [
    "# get multiple columns by name\n",
    "print(data[['CSD_code', 'Electronic_E']])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "combined-springer",
   "metadata": {},
   "outputs": [],
   "source": [
    "# make selection of rows based on a condition\n",
    "print(data[data['Electronic_E'] < -4000])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "voluntary-invention",
   "metadata": {},
   "outputs": [],
   "source": [
    "# make a selection of rows based on a condition and only query specific columns\n",
    "print(data[data['Electronic_E'] < -4000]['CSD_code'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "blessed-secretariat",
   "metadata": {},
   "outputs": [],
   "source": [
    "# TODO: get a selection of CSD codes of molecules with dipole moment < 1 \n",
    "# (Expected size: 9450 rows x 1 column)\n",
    "print(data[data['Dipole_M'] < 1]['CSD_code'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "particular-treasurer",
   "metadata": {},
   "outputs": [],
   "source": [
    "# TODO: get a selection of CSD codes and dispersion energies of molecules with dispersion energy < -0.01 \n",
    "# (Expected size: 13 rows x 2 columns)\n",
    "print(data[(data['Dispersion_E'] > -0.01)][['CSD_code', 'Dispersion_E']])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "under-concentrate",
   "metadata": {},
   "outputs": [],
   "source": [
    "# TODO: get a selection of electronic energies in the interval [-4000, -3000]\n",
    "# (Expected size: 20045 rows x 1 column)\n",
    "print(data[(data['Electronic_E'] > -4000) & (data['Electronic_E'] < -3000)]['Electronic_E'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "informative-athens",
   "metadata": {},
   "outputs": [],
   "source": [
    "# TODO: get a selection of CSD codes of molecules that either have HOMO energy > -0.1 or LUMO energy < -0.1\n",
    "# (Expected size: 27999 rows x 1 column)\n",
    "print(data[(data['HOMO_Energy'] > -0.1) | (data['LUMO_Energy'] < -0.1)]['CSD_code'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "married-skating",
   "metadata": {},
   "outputs": [],
   "source": [
    "# TODO: get a selection of CSD codes of molecules with metal center group = 3\n",
    "# (Expected size: 1389 rows x 1 column)\n",
    "print(data[data['Group'] == 3]['CSD_code'])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "young-laugh",
   "metadata": {},
   "source": [
    "#### Task 1: Basic statistics.\n",
    "##### Steps:\n",
    "1. Print the total number of data points and properties.\n",
    "2. Print basic statistical measures (mean, standard deviation) for some of the properties.\n",
    "3. Also print more properties such as max, min, median, etc.\n",
    "\n",
    "##### Hints:\n",
    "Use [numpy](https://numpy.org/) functions to calculate the stastical measures."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "adolescent-withdrawal",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# we can obtain the overall overall dimensions (i.e. shape) of the data frame\n",
    "data.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "timely-hamilton",
   "metadata": {},
   "outputs": [],
   "source": [
    "# calculate the mean of the HL gap\n",
    "print(np.mean(data['HL_Gap']))\n",
    "# TODO: calculate the standard deviation of the HL gap\n",
    "print(np.std(data['HL_Gap']))\n",
    "# TODO: calculate the maximum of the HL gap\n",
    "print(np.max(data['HL_Gap']))\n",
    "# TODO: calculate the minimum of the HL gap\n",
    "print(np.min(data['HL_Gap']))\n",
    "# TODO: calculate the median of the HL gap\n",
    "print(np.median(data['HL_Gap']))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6c913fc4",
   "metadata": {},
   "source": [
    "#### Task 2: Visualisation of data distributions - bar plots.\n",
    "Bar plots are used to visualize distributions of discrete variables such as the overall formal charge.\n",
    "##### Steps:\n",
    "1. Write a function to plot bar plots using [matplotlib.bar](https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.bar.html).\n",
    "2. Plot a bar plot for the charge. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0c26d44a",
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_bar(x: list, height: list, title: str, x_axis_label: str, y_axis_label: str):\n",
    "    \n",
    "    \"\"\"Plots a histogram of a specified data array/list.\n",
    "    \n",
    "    Args:\n",
    "        x (list): The x coordinates of the bars.\n",
    "        height (list): The heights of the bars.\n",
    "        title (str): The title of the plot.\n",
    "        x_axis_label (str): The label for the x axis (the property name).\n",
    "        y_axis_label (str): The label for the y axis (the height).\n",
    "    \"\"\"\n",
    "    \n",
    "    # TODO: implement bar plot.\n",
    "    plt.bar(x, height)\n",
    "    plt.title(title)\n",
    "    plt.xlabel(x_axis_label)\n",
    "    plt.ylabel(y_axis_label)\n",
    "\n",
    "    plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b0c056d6",
   "metadata": {},
   "outputs": [],
   "source": [
    "# collect unique charges and their respective occurrence\n",
    "charges = np.unique(data['Charge'].to_numpy().tolist())\n",
    "counts = []\n",
    "for charge in charges:\n",
    "    counts.append(len(data[data['Charge'] == charge]))\n",
    "\n",
    "plot_bar(charges, counts, 'Bar plot of charges', 'Charge', 'Number of occurrences')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "rising-qatar",
   "metadata": {},
   "source": [
    "#### Task 3: Visualisation of data distributions - histograms.\n",
    "Histograms are used to visualize distributions of continuous variables such as the HOMO-LUMO gap.\n",
    "##### Steps:\n",
    "1. Write a function to plot histograms using [matplotlib.hist](https://matplotlib.org/3.5.0/api/_as_gen/matplotlib.pyplot.hist.html).\n",
    "2. Plot histograms for the HOMO-LUMO gap, polarizability and dipole moment. Are they normal? If not, what are the shapes? What changes if you reduce/increase the number of bins?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "391a0e34",
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_hist(data: list, n_bins: int, title: str, x_axis_label: str):\n",
    "    \n",
    "    \"\"\"Plots a histogram of a specified data array/list.\n",
    "    \n",
    "    Args:\n",
    "        data (list): The data from which to build the histogram.\n",
    "        n_bins (int): The number of bins to use.\n",
    "        title (str): The title of the plot.\n",
    "        x_axis_label (str): The label for the x axis (the property name).\n",
    "    \"\"\"\n",
    "    \n",
    "    # TODO: implement histogram plot.\n",
    "    n, bins, patches = plt.hist(data, bins=n_bins)\n",
    "    plt.title(title)\n",
    "    plt.xlabel(x_axis_label)\n",
    "    plt.ylabel('Frequency')\n",
    "\n",
    "    plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "continuing-workstation",
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_hist(data['HOMO_Energy'], 100, 'Histogram of HOMO energies', 'HOMO Energy')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fatal-motor",
   "metadata": {},
   "source": [
    "#### Task 4: Visualisation of data correlations.\n",
    "##### Steps:\n",
    "1. Write a function to plot scatter plots of two variables using [matplotlib.scatter](https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.scatter.html).\n",
    "2. Plot a 2d scatter for the dispersion energy and polarizability.\n",
    "1. Write a modified scatter plot function that also calculates and plots a linear regression line and prints the slope, origin, and R² value. For the linear regression you can use the [numpy.polyfit](https://numpy.org/doc/stable/reference/generated/numpy.polyfit.html) and [numpy.poly1d](https://numpy.org/doc/stable/reference/generated/numpy.poly1d.html) functions from the [numpy](https://numpy.org/) package.\n",
    "4. Using this function, replot the 2d scatter for the dispersion energy and polarizability. (You should obtain an R² value of 0.952.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "019d6d45",
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_scatter(x: list, y: list, x_label: str, y_label):\n",
    "    \n",
    "    \"\"\"Plots a scatter of two provided lists of data points.\n",
    "    \n",
    "    Arguments:\n",
    "        x (list): The x data.\n",
    "        y (list): The y data.\n",
    "        x_label (list): The name associated to the x data.\n",
    "        y_label (list): The name associated to the y data.\n",
    "    \"\"\"\n",
    "    \n",
    "    # TODO: implement scatter plot\n",
    "    plt.scatter(x, y, s=2)\n",
    "    \n",
    "    plt.xlabel(x_label)\n",
    "    plt.ylabel(y_label)\n",
    "    plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "center-chester",
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_scatter(data['Dispersion_E'], data['Polarizability'], x_label='Electronic Energy', y_label='Dispersion Energy')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "actual-start",
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_scatter_lin_reg(x: list, y: list, x_label: str, y_label):\n",
    "    \n",
    "    \"\"\"Plots a scatter of two provided lists and shows the corresponding linear regression.\n",
    "    \n",
    "    Arguments:\n",
    "        x (list): The x data.\n",
    "        y (list): The y data.\n",
    "        x_label (list): The name associated to the x data.\n",
    "        y_label (list): The name associated to the y data.\n",
    "    \"\"\"\n",
    "    \n",
    "    # get linear regression\n",
    "    z = np.polyfit(x, y, 1)\n",
    "    p = np.poly1d(z)\n",
    "    # calculate r^2\n",
    "    \n",
    "    m = np.mean(y)\n",
    "    r_squared = 1 - (np.sum(np.power(y - p(x), 2)) / np.sum(np.power(y - m, 2)))\n",
    "    \n",
    "    print('Regression line equation: ' + str(p))\n",
    "    print('Coefficient of determination (R²): ' + str(np.round(r_squared, decimals=3)))\n",
    "\n",
    "    # get min and max values\n",
    "    min_value = min(x)\n",
    "    max_value = max(x)\n",
    "    \n",
    "    # plot data points and regression line\n",
    "    plt.scatter(x, y, s=2)\n",
    "    plt.plot([min_value, max_value], [p(min_value), p(max_value)], \"r--\")\n",
    "    \n",
    "    plt.xlabel(x_label)\n",
    "    plt.ylabel(y_label)\n",
    "    plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "economic-astronomy",
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_scatter_lin_reg(data['Dispersion_E'], data['Polarizability'], x_label='Electronic Energy', y_label='Dispersion Energy')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f5096858",
   "metadata": {},
   "source": [
    "#### Task 5: Making publication-ready figures.\n",
    "The default settings of matplotlib usually do not yield figures that are fit for publication in a journal. In this task you can freely explore different styling options in the plots you have created so far to make publication-ready figures. \n",
    "##### Some useful starting points:\n",
    "1. Increase the thickness of the frame. Make sure that you also increase the width and length of the x and y axis ticks.\n",
    "2. Increase the font size of tick and axis labels.\n",
    "3. Experiment with different colors.\n",
    "4. Try adding LaTeX symbols in the title and axis labels."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c28a886d",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
