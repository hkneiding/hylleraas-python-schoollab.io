# Session 1: Introduction and plotting

This section is designed as a quick introduction to Python and Jupyter notebooks. It is not meant to be a comprehensive introduction to Python, but rather a quick overview of the most important features. 

## Path: Beginner
The *beginner* path is designed for anyone with little or absolutely no coding experience (in Python or any other language). While it is obviously impossible to both learn how to program and learn Python in a single day, the goal of this path is to give you a quick overview of the most important features of Python and Jupyter notebooks and to get you started with some basics. If you are a complete beginner, this path will most likely take you longer than the prescribed hour to complete. The notebook is designed as both an introduction and as a reference for you to refer back to later on.

## Path: Intermediate
The *intermediate* path is designed for anyone with some experience in programming using another language. If you are already familiar with Python, you may find the intermediate path too easy. Congratulations, you already know enough to follow the subsequent lectures in this series. If you are unsure whether the intermediate path is for you, you can always start by quickly perusing the beginner path and see if it feels too easy for you.