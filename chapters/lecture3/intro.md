# Session 3: Automating tasks

In this session, we will go through some examples where Python is used in combination with other software to automate various tasks. 


Lecture:
- List of ideas for automation, doing chores with python.
- Talk about problem solving strategy: How do I find the information I need?
- Googling for information
- ChatGPT
- ...

Notebooks for exercises:
- Automation of email sending
    - As an academic, there are instances of repeating 
      boring emails, for example when inviting to workshops.
    - This can be automated! (One should be careful, of course ... )
    - Excel, Outlook (UiO mail), SMTP (e.g., google mail)
- Extracting images from PDF file
    - PDF files are an important part of the research landscape!
    - Take a journal article, extract figures etc. automatically
      and save to files. 
- Automate running quantum chemistry software
    - Running many numerical calculations in a batch and
      processing their results, putting numbers in an Excel sheet,
      can be a big and labor-intensive task.
    - Hylleraas Platform is the perfect tool for this!




Here are some other ideas:
- Read an Excel sheet into a Pandas dataframe.
    - Useful for further processing, like plotting, sending emails ...
    - Also, modify content and update the Excel sheet.
- Generate emails in Outlook.
    - One can use smtp to send mails with, say, gmail, but for Exchange, we must use the Outlook API, which is great.
- Rename a batch of image files based on metadata.
    - Say you have different sources of images (cameras, phones ...), and you
    want to sort images based on, say, capture date, for making a photo album
    with dumb software. We can make a script that renames each image file based on the
    creation time, which is stored in EXIF data.
- Interface ChatGPT
- Searching google with Python.
    - Do a google search, returing 1000 results, find all results that match certain criteria
- Checking for plagiarism, difflib.SequenceMatcher
    - A very simple check, not foolproof, but still potentialy useful.
- Generate secure passphrases
    - xkcd-style random combinations of 4 words. This is perhaps more a programming exercise, since tools are readily found on the internet.
- Convert image to jpg
    - Small script that converts an image to jpg.
- Extract text from an image
    - Tesseract/pytessearct/pillow
- Extracting data from a PDF/EPS plot
    - Use libraries to read a PDF/EPS, find the curves, extract the x and y coordinates
- Extracting text and images from a PDF file
    - In a PDF, images are stored as JPGs. Saving these images using, say, screenshots, will reduce the quality. It is better to simply extract the JPG images directly!
    - Copying text from a PDF can be difficuly. But we can dump all the text directly to the console.
- Convert tabular data from a PDF file to an array
    - Or even a JPG photo of a table in a book
- Converting an Excel table to a LaTeX table
- Symbolic algebra: computing integrals, differentiation, checking your calculations
    - Sympy
- Benford's law on tabular data
    - Check for false data by using Benford's law
- Generate input files to gaussian and other software, xtb,







