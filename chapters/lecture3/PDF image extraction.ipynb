{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "e636473f",
   "metadata": {},
   "source": [
    "# Extracting bitmap images from PDFs\n",
    "\n",
    "## Introduction\n",
    "\n",
    "PDFs are an important part of the workflow of the modern academic: From creating figures, writing and submitting manuscripts, letters, ... In this exercise, we will put together a notebook that helps extracting bitmap images from PDF files. This helps you get a feel for how you can manipulate PDFs using Python. This can be surprisingly useful.\n",
    "\n",
    "We will use the package `PyMuPDF`, which must be installed before you can proceed. On my bare-bones Anaconda distribution, I simply ran `pip install --upgrade pymupdf`, and I was good to go!\n",
    "\n",
    "For the documentation of `PyMuPDF`, see https://pymupdf.readthedocs.io/en/latest/. For a useful repos containing scripts and examples, see https://github.com/pymupdf/PyMuPDF-Utilities/tree/master.\n",
    "\n",
    "## What you will need\n",
    "\n",
    "Go get a PDF, for example an article, that contains bitmap figures. Look for photos. In the Python course repos, I have added `balcells_skjelstad_tmQM.pdf` (https://doi.org/10.1021/acs.jcim.0c01041), which contains several figures."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3a97c09a",
   "metadata": {},
   "source": [
    "##  Exercises\n",
    "### 1. Import required packages\n",
    "\n",
    "In the first cell, we will import the PyMuPDF package, which is named `fitz`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6ae72e86",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import the fitz package\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "981ca8a2",
   "metadata": {},
   "source": [
    "### 2. Open a PDF for reading.\n",
    "\n",
    "The next step is to open a PDF. The function to use is `fitz.open()`. \n",
    "\n",
    "Assign the output of `fitz.open( ... )` to the variable `doc`. Use either the online documentation or `help(fitz.open)` to find out how to select which file top open. Tip: In Jupyter Notebook, you can use the TAB key to autocomplete filenames. This is helpful if your file is not in the current folder."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "38ac3793",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Open docment and save results in the variable `doc`\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7ffcb6f8",
   "metadata": {},
   "source": [
    "### 3. Having a look at a page\n",
    "\n",
    "We now assume that you successfully assigned the PDF document to the `doc` variable.\n",
    "\n",
    "In the next cell, we demonstrate some useful concepts. We extract the first page of the PDF. The `Page` class contains a member function called `get_images()` that extracts information about the images inside the page. We then extract the first image (assuming it exists). Next, we want to display that image in the notebook. For that we use `PIL`, the Pyhthon Imaging Library. It is installed alongside `PyMuPDF`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bdc76f11",
   "metadata": {},
   "outputs": [],
   "source": [
    "import io\n",
    "from PIL import Image\n",
    "\n",
    "# Get the first page of the document\n",
    "page = doc[0]\n",
    "print(page)\n",
    "\n",
    "# Get the first image on this page.\n",
    "images = page.get_images()\n",
    "print(f'There are {len(images)} images on the page.')\n",
    "an_image = images[0]\n",
    "print(an_image)\n",
    "\n",
    "# Get the xref of the image, and extract the image from the document. This is fairly low level, but how it's done.\n",
    "xref = an_image[0]\n",
    "pixmap = fitz.Pixmap(doc, xref)\n",
    "print(pixmap)\n",
    "\n",
    "# Next, create a PIL Image instance of the pixmap.\n",
    "data = pixmap.tobytes()\n",
    "stream = io.BytesIO(data)\n",
    "pil_image = Image.open(stream)\n",
    "\n",
    "# Display and save\n",
    "display(pil_image)\n",
    "pil_image.save('an_image.png')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5a84e50e",
   "metadata": {},
   "source": [
    "### 4. Writing a helper function\n",
    "\n",
    "The above sequence of commands is complicated and can be confusing if we have to write them several times. Let us therefore program a helper function. In the below cell, we have started a function definition, and using the code from the previous cell, you should complete it.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fc09b3f1",
   "metadata": {},
   "outputs": [],
   "source": [
    "def view_and_save(an_image, filename):\n",
    "    \"\"\" Function that displays and saves an image.\n",
    "    \n",
    "    Args:\n",
    "    * an_image: An element from the output of fitz.Page.get_images()\n",
    "    * filename: Filename of saved file\n",
    "    \n",
    "    Returns: None.\n",
    "    \"\"\"\n",
    "    \n",
    "    # Write your code here\n",
    "    \n",
    "    print(f'Saved image to file {filename}.')\n",
    "\n",
    "    \n",
    "    \n",
    "# Test the function.\n",
    "view_and_save(an_image, 'is_this_working.png')\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3df9e21a",
   "metadata": {},
   "source": [
    "### 5. Looping over the document pages\n",
    "\n",
    "In the next step, we assume that the output of `fitz.open()` is assigned to `doc`. This is now a `Document` instance, and it is an **iterable** that we can loop over, just like a list.\n",
    "\n",
    "The following code loops over the pages, but does nothing with the document. (Run the cell with SHIFT+ENTER to see its output.)\n",
    "\n",
    "Inside the loop, the variable `page` contains a page of the document.\n",
    "\n",
    "Modify the loop body such that it extracts *all* images from each page, views and saves them.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a9ffb36d",
   "metadata": {},
   "outputs": [],
   "source": [
    "numpages = len(doc)\n",
    "print(f'The document contains {numpages} pages.')\n",
    "\n",
    "for i in range(numpages):\n",
    "    page = doc[i]\n",
    "    print(page)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "82a2e541",
   "metadata": {},
   "source": [
    "### 6. Challenge for the more experienced programmer\n",
    "\n",
    "It could be handy to have the above procedure as a script that can be called on the command line. Write a *script* that takes a PDF file as input via a command line argument and outputs all the images named consequently. For example\n",
    "\n",
    "`$ pdfimages myfile.pdf`\n",
    "\n",
    "should write all the bitmap images in `myfile.pdf` as `myfile-0.png`, `myfile-1.png`, etc.\n",
    "\n",
    "Here the standard package `argparse` can be useful. Make the script executable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ca8a67b1",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
