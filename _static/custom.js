// This script adds the 'download' attribute to all links ending with '.ipynb'
document.addEventListener('DOMContentLoaded', (event) => {
  document.querySelectorAll('a[href$=".ipynb"]').forEach((link) => {
    link.setAttribute('download', '');
  });
});
